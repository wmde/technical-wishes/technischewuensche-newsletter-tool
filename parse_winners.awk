BEGIN {
	count=0
	wishcount = 0
	pingcount=0
	ping="{{Ping"
	massmessage=""
	links=""
	templ = "\n<a href=\"https://de.wikipedia.org/w/index.php?title=%s%s&action=edit&section=new\">%s</a>,"
	templjs = "'%s%s', "
	js = "function openall10() { users=["
}

function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
function trim(s) { return rtrim(ltrim(s)); }
function clean(s) { gsub(/["']/, "", s); return rtrim(ltrim(s)); }

func printwish() {
		print("<h3 id=\"" clean(title) "\">" title "</h3>")
		print("Voters: " count)
		print("<br/><br/>\n")

		print("<div class=\"links\">")
		print(links)
		print("</div><br/>\n")
		print("&#128150;<a href=\"#\" onclick=\"openall1" wishcount "()\"><b>Alle in Tabs öffnen:</b></a>&#128150;")
		print("<script type=\"text/javascript\">" js "</script>")
		print("<br/><br/>\n")

		print("<details class=\"ping\"><summary>✨Ping</summary>")
		print(ping)
		print("</details><br/>\n")

		print("<details class=\"ping\"><summary>📜MassMessage</summary>")
		print(massmessage)
		print("</details><br/>")
	}

/===.*===/ {
	if(wishcount > 0) {
		ping=ping "}}"
		js = js "]; openall(users)}"
		printwish()
	}

	title=substr($0, 4, length($0)-7)

	wishcount += 1
	js = "function openall1" wishcount "() { links=["
	ping="{{Ping"
	massmessage=""
	links=""
	pingcount=0
	count=0
}

/^[^=]/ {
	count +=1 
	pingcount +=1 
	if(pingcount == 5) {
		pingcount=0
		ping=ping "}}{{Ping"		
	}
	ping=ping "|"$0
	massmessage=massmessage "{{target | user = " $0 " | site = de.wikipedia.org}}<br>"

	links = links sprintf(templ, "User_talk:", $0, $0)
	js    = js    sprintf(templjs, "User_talk:", $0)
}

END {
	ping=ping "}}"
	js = js "]; openall(links)}"
	printwish()
}
