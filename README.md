# technischewuensche-newsletter-tool

Tool to create and update the newsletter sending interface in `public_html/newsletter` including generated lists of users from
different sources.

- `updatenewsletter.sh` - script that should be run by a cron job to update the interface with the
newest subsribers.
- `survey-participants/` - lists with participants from the past survey winners
