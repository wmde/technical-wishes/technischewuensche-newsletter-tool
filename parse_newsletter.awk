BEGIN {
	IGNORECASE = 1
	for (i=1; i<5; i++) {
		count[i]=0
		ping[i] = "{{Ping"
	    massmessage[i]=""
		links[i] = ""
		js[i] = "function openall" i "() { users=["
	}
	title[1] = "Neue Umfragen"
	title[2] = "Workshops & Treffen"
	title[3] = "Umgesetzte Wünsche"
	title[4] = "Feedbackrunden"
	templ = "\n<a href=\"https://de.wikipedia.org/w/index.php?title=%s%s&action=edit&section=new\">%s</a>,"
	templjs = "'%s%s', "
}

END {
	for (i=1; i<5; i++) {
		ping[i] = ping[i] "}}"
		js[i] = js [i] "]; openall(users)}"
		print("<h3 id=\"" title[i] "\">" title[i] "</h3>")
		print("Subscriber: " count[i])
		print("<br/><br/>\n")

		print("<div class=\"links\">")
		print(links[i])
		print("</div><br/>\n")
		print("&#128150;<a href=\"#\" onclick=\"openall" i "()\"><b>Alle in Tabs öffnen:</b></a>&#128150;")
		print("<script type=\"text/javascript\">" js[i] "</script>")
		print("<br/><br/>\n")

		print("<details class=\"ping\"><summary>✨Ping</summary>")
		print(ping[i])
		print("</details><br/>\n")

		print("<details class=\"ping\"><summary>📜MassMessage</summary>")
		print(massmessage[i])
		print("</details><br/>")
	}
}

func parse(i) {
	match($0, /(Spezial:Beiträge\/|User:|Benutzer:|Benutzerin:|BD:|Benutzer Diskussion:|Unsigned\||Unsigniert\||unsigned\||unsigniert\|)([^|]*)[|\]}]/, res)
	if(res[2] == "") {
		print($0) > "error"
	}
	else {
		count[i]++
		sub("/siglink", "", res[2])
		split(res[2], usr, "/")

		ping[i] = ping[i] "|" usr[1]
		massmessage[i]=massmessage[i] "{{target | user = " usr[1] " | site = de.wikipedia.org}}<br>"

		if( match(res[2], "/") == 0 )
			ns="User_Talk:"
		else
			ns=res[1]
		links[i] = links[i] sprintf(templ, ns, res[2], usr[1])
		js[i] = js[i] sprintf(templjs, ns, res[2])
		if(count[i] % 5 == 0) {
			ping[i] = ping[i] "}}{{Ping"
		}
	}
}

/{{ja}} {{(ja|nein)}} {{(ja|nein)}} {{(ja|nein)}}/ {
	parse(1)
}

/{{(ja|nein)}} {{(ja)}} {{(ja|nein)}} {{(ja|nein)}}/ {
	parse(2)
}

/{{(ja|nein)}} {{(ja|nein)}} {{(ja)}} {{(ja|nein)}}/ {
	parse(3)
}

/{{(ja|nein)}} {{(ja|nein)}} {{(ja|nein)}} {{(ja)}}/ {
	parse(4)
}

