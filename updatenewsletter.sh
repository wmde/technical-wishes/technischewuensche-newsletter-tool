#!/bin/bash 

LANG=en_US.UTF-8
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
set -o pipefail

cd /data/project/technischewuensche/newsletter/ || exit

if [[ $# -ne 0 ]] ; then
    echo "To many arguments"
    exit
fi

# Write everything to the index.html file
mkdir -p /data/project/technischewuensche/public_html/newsletter
exec 1>/data/project/technischewuensche/public_html/newsletter/index.html


# Write head and current datetime
cat /data/project/technischewuensche/newsletter/head.html
echo 'Stand: '
date

# Write table of content (static, hard coded)
echo "<nav><ul>"
echo '<li><a href="#Neue Umfragen">Neue Umfragen</a></li>'
echo '<li><a href="#Workshops & Treffen">Workshops &amp; Treffen</a></li>'
echo '<li><a href="#Umgesetzte Wünsche">Umgesetzte Wünsche</a></li>'
echo '<li><a href="#Feedbackrunden">Feedbackrunden</a></li>'
echo '<li><a href="#Neue Umfragen">Neue Umfragen</a></li>'
echo '<li><a href="#Workshops & Treffen">Workshops & Treffen</a></li>'
echo '<li><a href="#Umgesetzte Wünsche">Umgesetzte Wünsche</a></li>'
echo '<li><a href="#Feedbackrunden">Feedbackrunden</a></li>'
echo '<li>Wünsche<ul>'
echo '<li><a href="#Vorschau von Einzelnachweisen">Vorschau von Einzelnachweisen</a></li>'
echo '<li><a href="#Bei Mehrfachreferenzierung Fußnoten-Link-Buchstaben hervorheben">Bei Mehrfachreferenzierung Fußnoten-Link-Buchstaben hervorheben</a></li>'
echo '<li><a href="#Bestimmung der Autoren eines Artikels (→ WikiHistory)">Bestimmung der Autoren eines Artikels (→ WikiHistory)</a></li>'
echo '<li><a href="#Autorenangaben unter den Artikeln">Autorenangaben unter den Artikeln</a></li>'
echo '<li><a href="#Geschützte Leerzeichen automatisch einsetzen">Geschützte Leerzeichen automatisch einsetzen</a></li>'
echo '<li><a href="#Sicherheitsabfrage bei Funktion ''kommentarlos zurücksetzen''">Sicherheitsabfrage bei Funktion ''kommentarlos zurücksetzen''</a></li>'
echo '<li><a href="#Geschlechterspezifische Anzeige der Kategorien im Artikel">Geschlechterspezifische Anzeige der Kategorien im Artikel</a></li>'
echo '<li><a href="#Wiedervorlagefunktion von Artikeln auf einen wählbaren Zeitpunkt">Wiedervorlagefunktion von Artikeln auf einen wählbaren Zeitpunkt</a></li>'
echo '<li><a href="#Automatisches speichern des Inhalts externer Links ins Internet Archive (archive.org)">Automatisches speichern des Inhalts externer Links ins Internet Archive (archive.org)</a></li>'
echo '<li><a href="#alle Artikel einer Kategorie beobachten">alle Artikel einer Kategorie beobachten</a></li>'
echo '</ul></li><li>Themenschwerpunkte<ul>'
echo '<li><a href="#Leichter mit Vorlagen arbeiten">Leichter mit Vorlagen arbeiten</a></li>'
echo '<li><a href="#Bessere Unterstützung von Geoinformationen">Bessere Unterstützung von Geoinformationen</a></li>'
echo '</ul></li>'
echo "</ul></nav>"

# call the api, parse output and generate the lists
url="https://de.wikipedia.org/w/api.php"
curl -s\
  -d "action=query"\
  -d "format=json"\
  -d "prop=revisions"\
  -d "rvprop=content"\
  -d "rvlimit=1"\
  -d "titles=Wikipedia:Technische_W%C3%BCnsche/Newsletter" \
"${url}" | \
	jq -r '.query .pages | .[] .revisions | .[0]["*"]' | \
	awk -f parse_newsletter.awk

# Additionally to the _newsletter_ this tool also presents the winnsers of the last surveys
# the `parse_winners` awk script takes a file with one user per line and generates
# the same html output as the newsletter script above
echo "<br><br><hr><br><br>"
echo "<h2>Topwünsche 2017</h2>"

cat survey-participants/2017winners.txt | awk -f parse_winners.awk

cat survey-participants/topicarea_templates.txt | awk -f parse_winners.awk

cat survey-participants/topicarea_geo.txt | awk -f parse_winners.awk

cat /data/project/technischewuensche/newsletter/tail.html
